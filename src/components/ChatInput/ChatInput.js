import React from 'react';

import AttachmentIcon from '@material-ui/icons/Attachment';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import { IconButton } from '@material-ui/core';
import Picker from 'emoji-picker-react';

import styles from './ChatInput.module.css';

const ChatInput = ({ openEmoji, setOpenEmoji, input, setInput, sendMessage, sendFile, onSumbit }) => {
  const onEmojiClick = (event, emojiObject) => {
    setInput(input + emojiObject.emoji);
  };

  return (
    <div className={styles.chatInput}>
      <form onSumbit={onSumbit}>
        <input type="file" id="fileid" accept="image/*" onChange={sendFile} className={styles.btnInput} />
        <label htmlFor="fileid" >
          <IconButton color="black" aria-label="upload picture" component="span">
            <AttachmentIcon/>
          </IconButton>
        </label>
      </form>
      <IconButton color="black" aria-label="insert emoji" component="span" onClick={() => setOpenEmoji(!openEmoji)}>
        <InsertEmoticonIcon/>
      </IconButton>
      <form className={styles.formInput}>
        <input 
          className={styles.inputChat}
          value={input}
          onChange={e => setInput(e.target.value)}
          placeholder='Type...' 
          type='text' 
        />
        <button 
          className={styles.btnInput}
          onClick={sendMessage}
        />
      </form>
      {openEmoji ? 
        <div className={styles.emojiPicker}>
          <Picker onEmojiClick={onEmojiClick} />
        </div>
      :
        null
      }
    </div>
  );
}

export default ChatInput;