import React from 'react';

import SideBar from '../SideBar/SideBar';
import Chat from '../Chat/Chat';

import styles from './FireChat.module.css';

const FireChat = () => {
  return (
    <div className={styles.firechat}>
      <SideBar/>
      <Chat />
    </div>
  );
}

export default FireChat;