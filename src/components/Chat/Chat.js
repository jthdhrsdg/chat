import React, { useEffect, useState } from 'react';

import ChatInput from '../ChatInput/ChatInput';
import ChatHeader from '../ChatHeader/ChatHeader';
import Messages from '../Messages/Messages';
import CircularProgress from '@material-ui/core/CircularProgress';

import { useSelector } from 'react-redux';
import { selectChatId, selectChatName } from '../../features/chatSlice';
import db from '../../firebase/firebase';
import { storage } from '../../firebase/firebase';
import firebase from 'firebase';
import { selectUser } from '../../features/userSlice';

import styles from './Chat.module.css';

const Chat = () => {
  const [input, setInput] = useState('');
  const [messages, setMessages] = useState([]);
  const chatName = useSelector(selectChatName);
  const chatId = useSelector(selectChatId);
  const user = useSelector(selectUser);
  const [loading, setLoading] = useState(true);
  const [openEmoji, setOpenEmoji] = useState(false);

  useEffect(() => {
    setLoading(false);
    setInput('');
    if (chatId) {
      return db.collection('chats')
        .doc(chatId)
        .collection('messages')
        .orderBy('timestamp', 'asc')
        .onSnapshot(snapshot => 
          setMessages(
            snapshot.docs.map(doc => ({
              id: doc.id,
              data: doc.data(),
            }))
        ))
    }
  }, [chatId]);

  const sendMessage = e => {
    e.preventDefault();

    if(input.length > 0 && chatId) {
      db.collection('chats')
      .doc(chatId)
      .collection('messages')
      .add({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        message: input,
        uid: user.uid,
        photo: user.photo,
        email: user.email,
        displayName: user.displayName,
      })
    }

    setOpenEmoji(false);
    setInput('');
  }

  const sendFile = e => {
    const file = e.target.files[0];
    storage
      .ref()
      .child('images/'+(+new Date()))
      .put(file)
      .then((snapshot) => {
        snapshot.ref.getDownloadURL()
        .then(url => 
          db.collection('chats')
          .doc(chatId)
          .collection('messages')
          .add({
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            picture: url,
            uid: user.uid,
            photo: user.photo,
            email: user.email,
            displayName: user.displayName
          })  
        )
      })
      .catch(err => err); 
  }

  const onSumbit = e => {
    e.preventDefault();
  }

  if (loading) return <CircularProgress color="inherit" />

  const selectChatMessage = 'Select a chat...';

  return (
    <>
      {chatId ?
        <div className={styles.chat}>
          <ChatHeader chatName={chatName} />
          <Messages messages={messages} /> 
          
          <ChatInput 
            input={input} 
            setInput={setInput} 
            sendMessage={sendMessage}
            sendFile={sendFile}
            onSumbit={onSumbit}
            openEmoji={openEmoji}
            setOpenEmoji={setOpenEmoji}
          />
         
        </div>
      :
        <h2 className={styles.chatMessage}>{selectChatMessage}</h2>
      }

    </>
  );
}

export default Chat;
