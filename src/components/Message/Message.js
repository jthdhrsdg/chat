import React, { useState } from 'react';

import { Lightbox } from "react-modal-image";
import { Avatar } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { selectUser } from '../../features/userSlice';

import styles from './Message.module.css';
import cn from 'classnames';

const Message = ({ id, data }) => {
  const user = useSelector(selectUser);
  const myMessage = (data.email === user.email) ? cn(styles.message, styles.messageMine) : styles.message; 
  const [clicked, setClicked] = useState(false);

  const onClickPic = () => {
    setClicked(!clicked);
  }

  return (
    <div className={myMessage}>
      {clicked && <Lightbox 
        large={data.picture} 
        onClose={onClickPic}
      />}
      <Avatar 
        className={styles.messagePhoto} 
        src={data.photo} 
      />
      {data.picture ? 
        <img 
          src={data.picture} 
          className={styles.messagePicture} 
          onClick={onClickPic}
        /> :
          <p className={styles.messageText}>{data.message || ''}</p>
        }
      <small className={styles.messageDate}>{new Date(data.timestamp?.toDate()).toLocaleString()}</small>
    </div>
  );
}

export default Message;