import React, { useState } from 'react';
import { selectUser } from '../../features/userSlice';
import { auth, provider } from '../../firebase/firebase';
import { Button } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import styles from './Login.module.css';
import logo from '../../assets/fire.png';

const Login = () => {
  const [error, setError] = useState(false);
  const user = useSelector(selectUser);

  const signIn = () => {
    auth.signInWithPopup(provider)
    .catch(err => setError(true));
  }

  if (user) {
    return <Redirect to='/chat' />;
  }

  const errMsg = 'Someting went wrong';
  return (
    <div className={styles.login}>
      {error ? <h2>{errMsg}</h2> 
      : 
        <>
        <div className={styles.logo}> 
          <img
            className={styles.loginImg}
            src={logo}
            alt='logo'
          />
          <h1>Fire Chat</h1>
        </div>
        <Button className={styles.loginBtn} onClick={signIn}>Sign In</Button>
        </>
      }
    </div>
  );
}

export default Login;