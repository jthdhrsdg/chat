import React from 'react';

import styles from './NotFoundPage.module.css';

const NotFoundPage = () => <div className={styles.notFoundPage}> <h2>404 page not found...</h2></div>;

export default NotFoundPage;
