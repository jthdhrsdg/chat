import React, { useEffect, useState } from 'react';

import { deleteChatById } from '../../features/chatSlice';
import { useDispatch } from 'react-redux';
import { setChat } from '../../features/chatSlice';
import db from '../../firebase/firebase';
import * as timeago from 'timeago.js';

import styles from './SideBarChat.module.css';

const SideBarChat = ({ id, chatName }) => {
  const dispatch = useDispatch();
  const [chatInfo, setChatInfo] = useState([]);

  useEffect(() => {
    fetchChatInfo(id);
  }, [id]);

  const fetchChatInfo = id => {
    db
      .collection('chats')
      .doc(id)
      .collection('messages')
      .orderBy('timestamp', 'desc')
      .onSnapshot(snapshot => 
        setChatInfo(snapshot.docs.map(doc => doc.data()))
      )
  };

  const selectChat = () => {
    dispatch(
      setChat({
        chatId: id,
        chatName: chatName,
      })
    )
  };

  const deleteChat = (id) => {
    dispatch(deleteChatById(id));
  }

  const chatMessage = chatInfo? chatInfo[0]?.message || 'media' : 'empty';
  const dateInfo = timeago.format(new Date(chatInfo[0]?.timestamp?.toDate())) || '';
  
  return (
    <div className={styles.sidebarChat} onClick={selectChat}>
      <div className={styles.info}>
        <div className={styles.infoText}>
          <h3>{chatName || ' '}</h3>
          <span>{chatMessage}</span>
        </div>
        <small className={styles.time}>{dateInfo}</small>
      </div>
      <div className={styles.deleteBtn} onClick={() => deleteChat(id)}/>
    </div>
  );
}

export default SideBarChat;