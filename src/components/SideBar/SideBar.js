import React, { useEffect, useState } from 'react';

import SideBarChat from '../SideBarChat/SideBarChat';
import SideBarHeader from '../SideBarHeader/SideBarHeader';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch } from 'react-redux';
import { fetchChats, selectChats } from '../../features/chatSlice';
import  toLowerCase  from '../../helpers/toLowerCase';
import db from '../../firebase/firebase';
import { useSelector } from 'react-redux';

import styles from './SideBar.module.css';

const SideBar = () => {
  const [search, setSearch] = useState('');
  const [filterChats, setFilterChats] = useState([]);
  const dispatch = useDispatch();
  const { chats }  = useSelector(selectChats);


  useEffect(() => {
    dispatch(fetchChats());
    setFilterChats(chats);
  }, [dispatch]);

  useEffect(() => {
    filteredChats(search);
  }, [search]);

  const addChat = () => {
    const chatName = prompt('Enter a room name');

    if (chatName) {
      db.collection('chats').add({
        chatName: chatName,
      });
    }
  };

  const filteredChats = search => {
    if (search.length > 0) {
      setFilterChats(chats.filter((chat) => 
        toLowerCase(chat.chatName).includes(toLowerCase(search))
      ));
    } else {
      setFilterChats(chats);
    }
  };

  const renderChats = () => {
    if (chats.loading) return  <CircularProgress color='black' />;
    if (chats.hasErrors) return <h2>Cannot display chats...</h2>;

    const resChats = (search.length > 0 && filterChats.length > 0) ? filterChats : chats;
    
    return (resChats.length > 0) ? resChats.map(({ id, chatName }) => 
      <SideBarChat 
        key={id}
        id={id}
        chatName={chatName}
      />
    ) : null
  }

  return (
    <div className={styles.sidebar}>
      <div className={styles.header}> 
        <SideBarHeader 
          search={search}
          setSearch={setSearch}
          addChat={addChat} 
        />
      </div>
      
      <div className={styles.chats}> 
        {renderChats()}
      </div>
    </div>
  );
}

export default SideBar;