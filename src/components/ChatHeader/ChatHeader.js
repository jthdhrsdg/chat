import React from 'react';

import styles from './ChatHeader.module.css';

const ChatHeader = ({ chatName, chatId }) => {
  return (
    <div className={styles.header}>
      <h3 id="title">Chat: <span className={styles.name}>{chatName || ''}</span></h3>
    </div>
  );
}

export default ChatHeader;