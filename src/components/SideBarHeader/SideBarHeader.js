import React from 'react';

import { Avatar, IconButton } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import CreateIcon from '@material-ui/icons/Create';

import { selectUser } from '../../features/userSlice';
import { auth } from '../../firebase/firebase';
import { useSelector } from 'react-redux';

import styles from './SideBarHeader.module.css';

const SideBarHeader = ({ search, setSearch, addChat }) => {
  const user = useSelector(selectUser);
  
  return (
    <div className={styles.sidebarHeader}> 
      <Avatar 
        className={styles.avatar}
        src={user.photo}
        onClick={() => auth.signOut()}
      />
      <div className={styles.inputField}> 
        <SearchIcon />
        <input 
          id="sidebarInput"
          type="text"
          placeholder='Search...' 
          onChange={e => setSearch(e.target.value) }
          search={search}
        />
      </div>

      <IconButton>
        <CreateIcon onClick={addChat}/>
      </IconButton>
    </div>
  );
}

export default SideBarHeader;