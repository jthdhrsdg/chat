import React from 'react';
import AlwaysScrollToBottom from '../AlwaysScrollToBottom/AlwaysScrollToBottom';
import Message from '../Message/Message';

import styles from '../Messages/Messages.module.css';

const Messages = ({ messages }) => {

  return (
    <div className={styles.messages}>
      {messages.length > 0 ? messages.map(({ id, data }) => <Message key={id} data={data} />) : null}
      <AlwaysScrollToBottom />
    </div>
  );
}

export default Messages;
