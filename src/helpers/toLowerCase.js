export default function toLowerCase(input) {
  return input.split(' ').map(item => item[0].toUpperCase() + item.substring(1)).join(' ');
}
