import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import FireChat from './components/FireChat/FireChat';

const PrivateRoute = ({ component: RouteComponent, currentUser, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(rrr) =>
        currentUser ? (
          <FireChat />
        ) : (
          <Redirect to={'/login'} />
        )
      }
    />
  );
};


export default PrivateRoute;