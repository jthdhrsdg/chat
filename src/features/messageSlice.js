import { createSlice } from '@reduxjs/toolkit';

import db from '../firebase/firebase';

export const messageSlice = createSlice({
  name: 'message',
  initialState: {
    loading: false,
    hasErrors: false,
    messages: [],
  },
  reducers: {
    getMessages: (state) => {
      state.loading = true;
    },
    getMessagesSuccess: (state, action) => {
      state.loading = false;
      state.hasErrors = false;
      state.chats = action.payload; 
      console.log(state.messages)
    },
    getMessagesFailure: (state) => {
      state.loading = false;
      state.hasErrors = true;
    },
  },
})


export function fetchMessages(id) {
  return async (dispatch) => {
    dispatch(getMessages())

    try {
       db
        .collection('chats')
        .doc(id)
        .collection('messages')
        .orderBy('timestamp', 'asc')
        .onSnapshot(snapshot => {
          const postChats = [];
          snapshot.forEach((doc) => {
             postChats.push({ ...doc.data(), id: doc.id })
          });
          dispatch(getMessagesSuccess(postChats));
        })
    } catch (error) {
      dispatch(getMessagesFailure())
    }
  }
}


export const { getMessages, getMessagesSuccess, getMessagesFailure } = messageSlice.actions;

export const selectMessages = state => state.message;


export default messageSlice.reducer;