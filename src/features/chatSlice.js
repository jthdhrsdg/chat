import { createSlice } from '@reduxjs/toolkit';

import db from '../firebase/firebase';

export const chatSlice = createSlice({
  name: 'chat',
  initialState: {
    loading: false,
    hasErrors: false,
    chatId: null,
    chatName: null,
    chats: [],
  },
  reducers: {
    setChat: (state, action) => {
      state.chatId = action.payload.chatId;
      state.chatName = action.payload.chatName;
    },
    getChats: (state) => {
      state.loading = true;
    },
    getChatsSuccess: (state, action) => {
      state.loading = false;
      state.hasErrors = false;
      state.chats = action.payload; 
      console.log(state.chats)
    },
    getChatsFailure: (state) => {
      state.loading = false;
      state.hasErrors = true;
    },
    deleteChat: (state, action) => {
      state.chats = state.chats.filter(chat => chat.chatId !== action.payload);
    }
  },
})


export function fetchChats() {
  return async (dispatch) => {
    dispatch(getChats())

    try {
       return db
        .collection('chats')
        .onSnapshot(snapshot => {
          const postChats = [];
          snapshot.forEach((doc) => {
             postChats.push({ ...doc.data(), id: doc.id })
          });
          dispatch(getChatsSuccess(postChats));
        })
    } catch (error) {
      dispatch(getChatsFailure())
    }
  }
}

export function deleteChatById(id) {
  return async (dispatch) => {
    db
      .collection('chats')
      .doc(id)
      .collection('messages')
      .delete()
      .then(res => {
        console.log('Chat deleted successfully');
        dispatch(deleteChat(id));
      })
      .catch(err => err);
    // db
    //   .collection('chats')
    //   .doc(id)
    //   .delete()
    //   .then(res => {
    //     console.log('Chat deleted successfully');
    //     dispatch(deleteChat(id));
    //   })
    //   .catch(err => err);
  }
}


export const { deleteChat, setChat, addChat, getChats, getChatsSuccess, getChatsFailure } = chatSlice.actions;

export const selectChatName = state => state.chat.chatName;
export const selectChatId = state => state.chat.chatId;
export const selectChats = state => state.chat;


export default chatSlice.reducer;