import firebase from 'firebase';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyBWZZKbL_HmZEFoiyWT3gfkMOsCgyNaiU8",
  authDomain: "fire-chat-2a690.firebaseapp.com",
  projectId: "fire-chat-2a690",
  storageBucket: "fire-chat-2a690.appspot.com",
  messagingSenderId: "951783742962",
  appId: "1:951783742962:web:459a1457ff22a441271f6b",
  measurementId: "G-5HZ7BXSJWN"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider, storage };
export default db;