import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { selectUser, login, logout } from './features/userSlice';
import { auth } from './firebase/firebase';

import FireChat from './components/FireChat/FireChat';
import Login from './components/Login/Login';
import PrivateRoute from './PrivateRoute';
import NotFoundPage from './components/NotFoundPage/NotFoundPage';

import styles from './App.module.css';

const App = () => {
  const user = useSelector(selectUser);
  const dispatch = useDispatch();

  useEffect(() => {
    auth.onAuthStateChanged(authUser => {
      authUser ? 
        dispatch(login({
          uid: authUser.uid,
          photo: authUser.photo || authUser.photoURL,
          email: authUser.email,
          displayName: authUser.displayName,
        })) : 
        dispatch(logout());
    })
  }, []);


  console.log('somethif');

  return (
    <div className={styles.app}>
      <Redirect to='/login' />
      <Switch>
        <PrivateRoute exact path='/chat' currentUser={user} component={FireChat} />
        <Route exact path='/login' component={Login} />
        <Route exact path='*' component={NotFoundPage} />
      </Switch> 
    </div>
  );
}

export default App;